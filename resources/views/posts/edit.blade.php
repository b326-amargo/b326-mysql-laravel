
@extends('layouts.app')

@section('tabName')
    Edit Post
@endsection

@section('content')

	<form class="p-5 mx-auto" method="POST" action="/posts/{{$post->id}}/edit">

		@method('PUT')
		@csrf
		
		<div class="form-group">
			<label for = "title">Title:</label>
			<input type="text" name="title" class="form-control" id="title" placeholder="{{$post->title}}">
		</div>

		<div class="form-group">
			<label for = "content">Content:</label>
			<textarea class="form-control" name="content" id="content" rows =3 placeholder="{{$post->body}}"></textarea>
		</div>

		<div class="mt-2">
			<button class="btn btn-primary">Update Post</button>
		</div>
	</form>

@endsection