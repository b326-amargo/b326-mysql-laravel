<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
// to access the authenticated user via Auth facade

use App\Models\Post;
// to access the model for creating a post, along with the currently saved posts on the table
// can be checked on the currently migrated model on database folder

use App\Models\PostLike;
use App\Models\PostComment;

class PostController extends Controller
{
    public function logout(){
    	Auth::logout();
    	// will logout the currently logged-in user
    	// method comes from the installed auth package

    	return redirect('/login');
    	// no logout page, only redirected on register page
    }

    public function createPost(){
    	return view('posts.create');
    	// view is the method for accessing the views folder?, then the (dev created) posts folder containing the create.blade.php file
    }
    // action to return a view containing a form for a blog post creation

    public function savePost(Request $request){
    // argument captures the request, and saves it on the $request variable

        if(Auth::user()){
            $post = new Post;
            // calling the Post model

            /* defining the properties of the $post using the form data from the request */
            $post->title = $request->input('title');
            // 'title' is the input id from the form
            $post->body = $request->input('content');
            $post->user_id = (Auth::user()->id);
            // will get the id of the authenticated user and set it as the foreign jet user_id of the new post
            $post->save();
            // will save the post object in the Post table

            return redirect('/posts/create');
        }
        // for checking if a user is currently authenticated

        else{
            return redirect('/login');
        }
    }

    public function showPosts(){
        $posts = Post::all();
        // all records on the Post table are saved on the variable

        return view('posts.showPosts')->with('posts', $posts);
        // create the new file on:
        // resources/views/posts/showPosts.blade.php
        // with() was to enable props on the showPosts file, 'posts' is the name of the prop, $posts is the value of the prop
    }
    // will return all saved blog posts

    /*===============================================*/

    /* S02 Activity */
    public function randomPosts(){
        $randomPosts = Post::where('isActive', 1)
                ->inRandomOrder()
                ->limit(3)
                ->get();

        return view('welcome')->with('randomPosts', $randomPosts);
    }

    /*===============================================*/

    public function myPosts(){
        if(Auth::user()){
            $posts = Auth::user()->posts;
            // laravel will cross-check the posts table and the current auth user, and save the results on the $variable
            
            return view('posts.showPosts')->with('posts', $posts);
        }
        else{
            return redirect('/login');
        }
    }
    // for showing only the posts authored by authenticated user

    public function show($id){
    // $id is defined from the url route

        $post = Post::find($id);
        $comments = PostComment::where('post_id', $id)->get();
        return view('/posts.show')
            ->with('post', $post)
            ->with('comments', $comments);
    }
    // action that returns a view showing a specific post using the url parameter $id to query for the database entry to be shown

    /*===============================================*/

    /* S03 Activity */
    public function edit($id){
        $post = Post::find($id);
        return view('/posts.edit')->with('post', $post);
    }

    public function updatePost(Request $request, $id){
        if(Auth::user()){
            $post = Post::find($id);

            $post->title = $request->input('title');
            $post->body = $request->input('content');
            $post->save();

            return redirect('/posts');
        }

        else{
            return redirect('/login');
        }
    }

    /*===============================================*/

    /* S04 Activity */
    public function archive($id){
        $post = Post::find($id);

        $post->isActive = false;
        $post->save();

        // if(Auth::user()->id == $post->user_id){
        //     $post->isActive = false;
        //     $post->save();
        // }

        return redirect('/posts');
    }

    /*===============================================*/

    public function like($id){
    // $id is post_id from post table
        $post = Post::find($id);

        if($post->user_id != Auth::user()->id){
        // will check if auth user is the author of the post (author is not allowed to like his own post)
            if($post->likes->contains('user_id', Auth::user()->id)){
            // if condition met (not the author), will check if auth user already liked the post
                Postlike::where('post_id', $post->id)
                    ->where('user_id', Auth::user()->id)
                    ->delete();
                // checked for the post id and the user id of the like record, then unliked the post by deleting the record 

            }
            else{
                $postlike = new Postlike;
                // if post is unliked, will create a new like record

                $postlike->post_id = $post->id;
                $postlike->user_id = Auth::user()->id;

                $postlike->save();
            }
        }

        return redirect("/posts/$id");
    }

    /*===============================================*/

    /* S05 Activity */
    public function comment(Request $request, $id){
        $comment = new PostComment;
        $post = Post::find($id);

        if(Auth::user()){
            $comment->content = $request->input('comment');
            $comment->post_id = $post->id;
            $comment->user_id = Auth::user()->id;
            $comment->save();
        }
        
        return redirect("/posts/$id");
    }

}
