<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_comments', function (Blueprint $table) {

            // Table Columns
            $table->bigIncrements('id');
            $table->text('content');
            $table->unsignedBigInteger('post_id'); // Foreign key
            $table->unsignedBigInteger('user_id'); // Foreign key
            $table->timestamps();

            // Constraints for Post Table Foreign Key
            $table->foreign('post_id') // foreign key
                ->references('id') // column "id"
                ->on('posts') // table "posts"
                ->onDelete('restrict')
                ->onUpdate('cascade');

            // Constraints for User Table Foreign Key
            $table->foreign('user_id') // foreign key
                ->references('id') // column "id"
                ->on('users') // table "users"
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_comments');
    }
};
