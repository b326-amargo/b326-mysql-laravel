<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // to defining one-to-many entity relationship between user and post model
    public function posts(){
        return $this->hasMany('App\Models\Post');
        // hasMany() since a user can have multiple posts
    }

    // to defining one-to-many entity relationship between user and postlike model
    public function likes(){
        return $this->hasMany('App\Models\PostLike');
        // hasMany() since a user can have likes on multiple posts
    }

    // s05 Activity
    public function comments(){
        return $this->hasMany('App\Models\PostLike');
        // hasMany() since a user can have likes on multiple posts
    }
}
