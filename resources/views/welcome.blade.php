
@extends('layouts.app')

@section('tabName')
    Welcome
@endsection

@section('content')

    <div class="mb-5">
        <img src="{{ asset('laravel-1-logo-png-transparent.png') }}" class="mx-auto d-block w-50">
    </div>
    
    <section>
        <h3 class="text-center">Featured Posts:</h3>
    
        @if(count($randomPosts)>0)
            @foreach($randomPosts as $randPost)
                <div class="card text-center mx-auto mt-2">
                    <div class="card-body">
                        <h4 class="card-title mb-3">
                            <a href="/posts/{{$randPost->id}}">
                                {{$randPost->title}}
                            </a>
                        </h4>
                        <h6 class="card-text mb-3">Author: {{$randPost->user->name}}</h6>
                    </div>
                </div>
            @endforeach
        @else
            <div>
                <h5 class="text-center mt-3">There are no posts to show.</h5>
            </div>
        @endif
    </section>
        
@endsection