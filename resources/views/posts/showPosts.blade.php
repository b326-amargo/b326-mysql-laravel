@extends('layouts.app')

@section('tabName')
    Newsfeed
@endsection

@section('content')
	<!-- <h1>{{$posts}}</h1> -->
	<!-- $posts is the props from the PostController file, will show all the posts as objects -->

	<h3>NEWSFEED</h3>

	@if(count($posts)>0)

		@foreach($posts as $post)

			@if($post->isActive == true)

				<div class="card text-center col-6 mx-auto mt-2">

					<div class="card-body">

						<h4 class="card-title mb-3">
							<a href="/posts/{{$post->id}}">
								{{$post->title}}
							</a>
						</h4>

						<h6 class="card-text mb-3">Author: {{$post->user->name}}</h6>
						<!-- laravel autosearches for the user column via foreign key of the post (user_id) -->

						<p class="card-subtitle mb-3 text-muted">Created at: {{$post->created_at}}</p>

						<p class="card-text">{{$post->body}}</p>

					</div>

					@if(Auth::user())
					<!-- to check if auth user is the author of this blog -->
						@if(Auth::user()->id == $post->user_id)
							<div class="card-footer container-fluid">
								<div class="d-flex flex-row">
									<form method="POST" class="d-flex flex-column col-4 mx-auto">
										<a href="/posts/{{$post->id}}/edit" class="btn btn-primary">Edit Post</a>
									</form>
									<form method="POST" class="d-flex flex-column col-4 mx-auto" action="/posts/{{$post->id}}">
										@method('PUT')
										@csrf
										<button class="btn btn-danger">Archive Post</button>
									</form>
								</div>
								
							</div>
						@endif
					@endif

				</div>
			@endif
		@endforeach

	@else
		<div>
			<h2>There are no posts to show.</h2>
			<a href="/posts/create" class="btn btn-info">Create a Post</a>
		</div>

	@endif

@endsection