<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostLike extends Model
{
    public function post(){
    	return $this->belongsTo('App\Models\Post');
    	// belongsTo() since each like is only directed to one post
    }

    public function user(){
    	return $this->belongsTo('App\Models\User');
    	// belongsTo() since each like is only directed to one user
    }
}
