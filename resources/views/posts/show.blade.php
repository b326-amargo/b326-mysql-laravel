
@extends('layouts.app')

@section('tabName')
    {{$post->title}}
@endsection

@section('content')
	<div class="card col-6 mx-auto mb-5">
		<div class="card-body">
			<h2 class="card-title">{{$post->title}}</h2>
			<p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
			<p class="card-subtitle text-muted mb-3">Created At: {{$post->created_at}}</p>
			<h4>Content:</h4>
			<p class="card-text">{{$post->body}}</p>

			@if(Auth::user())
			<!-- will show like/unlike and comment button if someone is logged in -->
				@if(Auth::id() != $post->user_id)
				<!-- Auth user is excempted from liking and commenting -->
					<form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
						@method('PUT')
						@csrf
						
						@if($post->likes->contains('user_id', Auth::id()))
						<!-- if current user already liked the post -->
						<!-- Postlike model was accessed via likes() function contained on the Post model (which is the model of $post), then searched if the auth user's id and the user_id of the like is similar -->
							<button class="btn btn-danger">Unlike</button>
						@else
						<!-- current user has not liked the post yet -->
							<button class="btn btn-success">Like</button>
						@endif
					</form>
				@endif

				<!-- Comment Button / Modal -->

				<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#commentModal">
				  Comment
				</button>

				<div class="modal" tabindex="-1" id="commentModal">
				  <div class="modal-dialog">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title">Leave a Comment</h5>
				        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				      </div>
				      <form class="modal-body" method="POST" action="/posts/{{$post->id}}/comment">
				      @method('POST')
				      @csrf
				      	<label for = "title">Comment:</label>
						<input type="text" name="comment" class="form-control" id="comment">
						<div class="modal-footer">
					      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
					      <button type="button submit" class="btn btn-primary">Comment</button>
				      	</div>
				      </form>
				      
				    </div>
				  </div>
				</div>

			@endif

			<a href="/posts" class="btn btn-info">View All Posts</a>

		</div>
	</div>

	<!-- Show All Comments Section -->

	<div class="container-fluid col-6">
		<h2>Comments:</h2>

		@if(count($comments)>0)
			@foreach($comments as $comment)
			<div class="card mx-auto mt-2 mb-2 text-center">
				<div class="card-body">
					<h2 class="card-title">{{$comment->content}}</h2>
					<p class="card-subtitle text-muted">Posted by: {{$comment->user->name}}</p>
					<p class="card-subtitle text-muted mb-3">Posted on: {{$comment->created_at}}</p>
				</div>
			</div>
			@endforeach

		@else
			<div class="mt-3 text-center">
				<h4>There are no comments to show.</h4>
			</div>

		@endif
	</div>
	

@endsection