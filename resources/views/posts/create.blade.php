@extends('layouts.app')
<!-- to extend the app.blade.php main page file -->

@section('tabName')
    Create Post
@endsection

@section('content')
<!-- inserts the next code lines after this on the <main> section on the app.blade.php file (around ines 78-80) -->

	<form class="col-3 bg-safe p-5 mx-auto" method="POST" action="/posts">
	<!-- method can only accept POST and GET -->

		@csrf
		<!-- stands for cross-site request forgery. To prevent malicious requests by hackers, via using tokens that will detect any tampers on the form input request. Also to prevent 419 error -->

		<div class="form-group">
			<label for = "title">Title:</label>
			<input type="text" name="title" class="form-control" id="title">
		</div>

		<div class="form-group">
			<label for = "content">Content:</label>
			<textarea class="form-control" name="content" id="content" rows =3></textarea>
		</div>

		<div class="mt-2">
			<button class="btn btn-primary">Create Post</button>
		</div>
	</form>

@endsection