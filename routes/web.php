<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\PostController;
// to enable access on needed file

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PostController::class, 'randomPosts']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');



/*==============*/

Route::get('/logout', [PostController::class, 'logout']);
// route for logging out
// '/logout' is the URI endpoint, PostController is the imported file name, 'logout' is the method from the class

Route::get('/posts/create', [PostController::class, 'createPost']);
// route for creating a post

Route::post('/posts', [PostController::class, 'savePost']);
// route for saving the post on the database

Route::get('/posts', [PostController::class, 'showPosts']);
// route for viewing all current posts

Route::get('/myPosts', [PostController::class, 'myPosts']);
// route that will return a view containing only the authenticated user's post

Route::get('/posts/{id}', [PostController::class, 'show']);
// route wherein a view showing a specific post with matching url parameter ID will be returned to the user

//=========================

// s03 activity, editing a post
Route::get('/posts/{id}/edit', [PostController::class, 'edit']);
Route::put('/posts/{id}/edit', [PostController::class, 'updatePost']);

//=========================

// s04 activity, archiving a post
Route::put('/posts/{id}', [PostController::class, 'archive']);

//=========================

Route::put('/posts/{id}/like', [PostController::class, 'like']);
// defining a route that will call the function for liking and unliking a specific post

//=========================

// s05 activity, commenting on a post
Route::post('/posts/{id}/comment', [PostController::class, 'comment']);